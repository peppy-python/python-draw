import turtle 

class Figure:
    pen = turtle.Pen()

    def __init__(self, color='black', width=1, angle=60, size=100):
        self.pen.color(color)
        self.pen.width(width)
        self.pen.speed(10000)
        self.angle = angle
        self.size = size

    def render(self, itters, is_right=True):
        itters = range(itters)

        turn = self.pen.right if is_right else self.pen.left

        for i in itters:
            self.pen.forward(self.size)
            turn(self.angle)
            
        turtle.done()


class DynamicFigure(Figure):
    def __init__(self, color='black', width=1, angle=60, size=100, diff=1):
        super().__init__(color=color, width=width, angle=angle, size=size)
        self.diff = diff

    def render(self, itters, is_right=True):
        itters = range(itters)
        size = self.size

        turn = self.pen.right if is_right else self.pen.left

        for i in itters:
            self.pen.forward(size)
            turn(self.angle)
            size-=self.diff
            
        turtle.done()


class ColorfulDynamicFigure(DynamicFigure):
    def __init__(self, color='black', width=1, angle=60, size=100, diff=1, colors=None):
        super().__init__(color=color, width=width, angle=angle, size=size, diff=diff)
        self.colors = colors if colors else []

    def render(self, itters, is_right=True):
        itters = range(itters)
        size = self.size

        turn = self.pen.right if is_right else self.pen.left

        for i in itters:
            self.pen.color(self.colors[i % len(self.colors)])
            self.pen.forward(size)
            turn(self.angle)
            size-=self.diff
            
        turtle.done()
