from figure import Figure, DynamicFigure, ColorfulDynamicFigure


figure = ColorfulDynamicFigure(width = 5, size = -7, diff = -1, angle = 57, colors=['blue','green','red'])
figure.render(10000)